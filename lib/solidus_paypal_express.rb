require 'solidus_core'
require 'solidus_paypal_express/version'
require 'solidus_paypal_express/engine'
require 'sass/rails'

module SolidusPaypalExpress
  def self.table_name_prefix
    'solidus_paypal_express_'
  end
end
